# Introduction à la bibliothèque MapboxGL.js

## MapboxGL

MapboxGL est une bibliothèque JavaScript de cartographie en ligne accessible via une API. Elle permet de développer des applications de cartographie en ligne basées sur les technologies des **tuiles vectorielles** et du **WebGL**.

**Documentation** > https://docs.mapbox.com/mapbox-gl-js/api/ 

**Exemples documentés proposés par Mapbox** https://docs.mapbox.com/mapbox-gl-js/example/ 

**Exemples de cartes** https://bl.ocks.org/mastersigat


## Template d'une carte

    <html>
    <head>
    <meta charset='utf-8' />
    <title>Display a map</title>
    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
    <style>
        body { margin:0; padding:0; }
        #map { position:absolute; top:0; bottom:0; width:100%; }
    </style>
    </head>
    
    <body>
    <div id='map'></div>
   
    <script>

    // AccesToken
    mapboxgl.accessToken = 'pk.eyJ1IjoibmluYW5vdW4iLCJhIjoiY2pjdHBoZGlzMnV4dDJxcGc5azJkbWRiYSJ9.o4dZRrdHcgVEKCveOXG1YQ';

    // Configuration de la carte
    var map = new mapboxgl.Map({container: 'map',
    style: 'mapbox://styles/mapbox/light-v9', // fond de carte
    center: [-1.68, 48.12], // lat/long
    zoom: 15, // zoom
    pitch: 50, // Inclinaison
    bearing: -10 // Rotation
    });

    </script>

    </body>
    </html>
    
![image-2.png](./images/Map1.JPG)


## Changer le fond de carte

### Utiliser un fond de carte déjà stylisé par Mapbox

Pour modifier me fond de carte il suffit de changer l'appel du **style**

Par exemple pour afficher le fond de carte Dark il suffit de le spécifier dans l'appel du fond de carte

        style: 'mapbox://styles/mapbox/dark-v9', // fond de carte

![image-2.png](./images/basemaps.JPG)

### Utiliser un fond de carte personnel produit dans MapboxStudio

Pour mobiliser un fond de carte crée dans MapboxStudio il suffit de spécifier dans l'appel du fond de carte l'URL de votre Style

![image-2.png](./images/mapboxstudio.JPG)



        style: 'mapbox://styles/ninanoun/ck3vlsdfx0upt1ck0uufam4uf', // fond de carte


### Utiliser un fond de carte en tuile vectorielle proposé par un fournisseur externe

Utilisons ici un service de fond de carte en tuiles vectorielles proposé par Etalab https://openmaptiles.geo.data.gouv.fr/

URL du fond de carte  > https://openmaptiles.geo.data.gouv.fr/styles/osm-bright/style.json

        style: 'https://openmaptiles.geo.data.gouv.fr/styles/osm-bright/style.json', // fond de carte


![image-2.png](./images/Map2.JPG)

## Ajouter des données d'OpenStreetMap

### Ajouter la couche de la voirie

L'idée est ici de mobiliser certaines données issues de la base OSM en mobilisant le jeu de données *mapbox-streets-v8* en tuiles vectorielles proposé par Mapbox
https://docs.mapbox.com/vector-tiles/reference/mapbox-streets-v8/ 

![image-2.png](./images/mapboxosm.JPG)

       
       // AJout de donnees OSM

          // Configuration de la source

        map.on('load', function () {
        map.addSource('mapbox-streets-v8', {
        type: 'vector',
        url: 'mapbox://mapbox.mapbox-streets-v8'});
        
          // Configuration de la couche de la voirie

        map.addLayer({
        "id": "Routes",
        "type": "line",
        "source": "mapbox-streets-v8",
        "layout": {'visibility': 'visible'},
        "source-layer": "road",
        "paint": {"line-color": "#FF7F50", "line-width": 1}
        });
        
        });

![image-2.png](./images/Map3.JPG)

### Ajouter la couche de l'hydrographie

En se basant sur la même source (ici mapbox-strets-v8) nous ajoutons la couche de l'hydrographie (*waterway*)

        // Configuration de la couche de l'hydrographie

        map.addLayer({"id": "hydrologie",
        "type": "line",
        "source": "mapbox-streets-v8",
        "source-layer": "waterway",
        "paint": {"line-color": "#4dd2ff",
        "line-width": 10}
        });

![image-2.png](./images/Map4.JPG)

### Ajouter la couche des POI 

En se basant sur la même source (ici mapbox-strets-v8) nous ajoutons la couche de l'hydrographie (*poi_label*)


          // Configuration de la couche des POI

         map.addLayer({"id": "transit",
         "type": "circle",
         "source": "mapbox-streets-v8",
         "source-layer": "poi_label",
         "paint": {"circle-color": "#ffffff",
         "circle-radius": 3}
         });

![image-2.png](./images/Map5.JPG)


<figure class="video_container">
<iframe src="https://jsfiddle.net/mastersigat/k85jrnxf/1/embedded/html,result/" </iframe>>
</figure>

